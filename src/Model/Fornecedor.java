package Model;

import java.util.ArrayList;

public class Fornecedor 
{
    private String nome;
    private ArrayList<String> telefones;
    private Endereco endereco;
    private ArrayList<String> produtos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public ArrayList<String> getTelefones() {
        return telefones;
    }

    public void setTelefones(ArrayList<String> telefones) {
        this.telefones = telefones;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<String> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<String> produtos) {
        this.produtos = produtos;
    }
    
    /*public void adicionarProduto(String nomeProduto)
    {
        produtos.add(nomeProduto);
    }
    */
}
