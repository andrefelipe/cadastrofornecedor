package Controller;

import Model.Fornecedor;
import Model.PessoaFisica;
import Model.PessoaJuridica;
import java.util.ArrayList;

public class ControleFornecedor 
{
    private ArrayList<Fornecedor> listaFornecedores;
    
    
    public String adicionar(PessoaFisica fornecedor)
    {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Fisica adicionado com sucesso";
    }
    
    public String adicionar(PessoaJuridica fornecedor)
    {
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Juridica adicionado com sucesso";
    }
    
    public Fornecedor pesquisar(String nome)
    {
        for (Fornecedor fornecedor : listaFornecedores)
        {
            if(fornecedor.getNome().equalsIgnoreCase(nome))
                return fornecedor;
        }
        return null;
    }
    
    public String remover(PessoaFisica fornecedor)
    {
        listaFornecedores.remove(fornecedor);
        return "Fornecedor Pessoa Fisica removido com sucesso";
    }
    
    public String remover(PessoaJuridica fornecedor)
    {
        listaFornecedores.remove(fornecedor);
        return "Fornecedor Pessoa Juridica removido com sucesso";
    }
}
